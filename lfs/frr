###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007-2023  IPFire Team  <info@ipfire.org>                     #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

SUMMARY    = FRRouting Routing daemon

VER        = 8.5.2

THISAPP    = frr-frr-$(VER)
DL_FILE    = $(THISAPP).tar.gz
DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)
TARGET     = $(DIR_INFO)/$(THISAPP)
PROG       = frr
PAK_VER    = 6

DEPS       = elfutils

SERVICES   = frr

CFLAGS    += -fcommon

###############################################################################
# Top-level Rules
###############################################################################

objects = $(DL_FILE)

$(DL_FILE) = $(DL_FROM)/$(DL_FILE)

$(DL_FILE)_BLAKE2 = 2e2aca4e42757f66c9ca4725826c6cc1d611930490eed2a175ca5b56910f2c09a9d842b2a9370a64a9fdac6a6314bd4573be609d14dbf956049d9fbf49310404

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

b2 : $(subst %,%_BLAKE2,$(objects))

dist:
	@$(PAK)

###############################################################################
# Downloading, checking, b2sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_BLAKE2,$(objects)) :
	@$(B2SUM)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) && cd $(DIR_SRC) && tar axf $(DIR_DL)/$(DL_FILE)
	$(UPDATE_AUTOMAKE)
	cd $(DIR_APP) && ./bootstrap.sh
	cd $(DIR_APP) && ./configure \
		--prefix=/usr \
		--sysconfdir=/etc/frr \
		--localstatedir=/var/run/frr \
		--enable-user="frr" \
		--enable-group="frr" \
		--enable-vty-group="frrvty" \
		--enable-multipath=64 \
		--disable-doc \
		--disable-babeld \
		--disable-bfdd \
		--disable-eigrpd \
		--disable-irdp \
		--disable-isisd \
		--disable-ldpd \
		--disable-nhrpd \
		--disable-ospf6d \
		--disable-ospfapi \
		--disable-pbrd \
		--disable-pimd \
		--disable-ripd \
		--disable-ripngd \
		--disable-static
	cd $(DIR_APP) && make $(MAKETUNING)
	cd $(DIR_APP) && make install

	# Install backup include
	install -v -m 644 $(DIR_SRC)/config/backup/includes/frr \
		 /var/ipfire/backup/addons/includes/frr

	# Install initscript
	$(call INSTALL_INITSCRIPTS,$(SERVICES))

	@rm -rf $(DIR_APP)
	@$(POSTBUILD)

