###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007-2023  IPFire Team  <info@ipfire.org>                     #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

VER        = 6.4.0

THISAPP    = iproute2-$(VER)
DL_FILE    = $(THISAPP).tar.xz
DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)
TARGET     = $(DIR_INFO)/$(THISAPP)

###############################################################################
# Top-level Rules
###############################################################################

objects = $(DL_FILE)

$(DL_FILE)             = $(DL_FROM)/$(DL_FILE)

$(DL_FILE)_BLAKE2 = 76369d2a794afd13dba1a34ee9a7425775bb7b43c22164274749c01b18d061669aa5abd51d32a15d68f48e75f4837a887dfc28ea41e34d8e4c36b2ab865628dd

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

b2 : $(subst %,%_BLAKE2,$(objects))

###############################################################################
# Downloading, checking, b2sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_BLAKE2,$(objects)) :
	@$(B2SUM)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) && cd $(DIR_SRC) && tar axf $(DIR_DL)/$(DL_FILE)
	cd $(DIR_APP) && make $(MAKETUNING) SBINDIR=/sbin
	cd $(DIR_APP) && make SBINDIR=/sbin install
	cd $(DIR_APP) && mv -v /sbin/arpd /usr/sbin

	# Add table for static routing
	echo "200	static" >> /etc/iproute2/rt_tables

	@rm -rf $(DIR_APP)
	@$(POSTBUILD)
